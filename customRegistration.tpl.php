<?php
/**
 * @file
 * Template file for the theming example text form.
 *
 * Available custom variables:
 * - $text_form: A string containing the pre-rendered form.
 * - $text_form_content: An array of form elements keyed by the element name.
 *
 * The default example below renders the entire form and its form elements in
 * a default order provided by Drupal.
 *
 * Alternatively, you may print each form element in the order you desire,
 * adding any extra html markup you wish to decorate the form like this:
 *
 * <?php print $text_form_content['element_name']; ?>
 *
 * The following snippet will print the contents of the $text_form_content
 * array, hidden in the source of the page, for you to discover the individual
 * element names.
 *
 * <?php print '<!--' . print_r($text_form_content, TRUE) . '-->'; ?>
 */
?>

    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="col-lg-12">
                    <?php print render($form['name']); ?>
                </div>
                <div class="col-lg-12">
                    <?php print render($form['mail']); ?>
                </div>
                <div class="col-lg-12">
                    <?php print render($form['mobile_number']); ?>
                </div>
                <div class="col-lg-12">
                        <?php print render($form['package'])?>
                </div>
                <div class="col-lg-12">
                    <?php print render($form['password'])?>
                </div>  
                <div class="col-lg-12">
                    <?php print render($form['passwordConfirmation'])?>
                 </div>                
<div class="col-lg-12">
                    <?php print render($form['customFormSubmit'])?>;
                </div>
            </div>
        </div>
    </div>
<?php print drupal_render_children($form); ?>
